# ------------------------------
# Dependencies
library(Matrix)
library(reticulate)
library(arrow)
library(rsvd)

# >> Set python interpreter (rely on pyenv and poetry)
use_python(Sys.which('python'), required=TRUE)

# >> Python dependencies (via reticulate)
geosketch <- import('geosketch')
skyteth_r <- import('skytether.skyteth_r')


# ------------------------------
# Test code

# reference directories and base names
ebi_dirpath  <- file.path('resources', 'sample-data', 'ebi')
mtx_basename <- file.path(ebi_dirpath
    ,'E-GEOD-100618'
    ,'E-GEOD-100618.aggregated_filtered_counts'
)


# >> function: ReadGeneExpr
partition_data <- skyteth_r$ReadMTX(
     mtx_filepath=paste(mtx_basename, 'matrix.mtx', sep='_')
    ,row_filepath=paste(mtx_basename, 'mtx_rows'  , sep='.')
    ,col_filepath=paste(mtx_basename, 'mtx_cols'  , sep='.')
)
partition_matrix <- partition_data$AsMatrix()


# >> function: DeduplicateGenes
src_genes <- partition_matrix$gene_id
dup_genes <- src_genes[duplicated(src_genes$as_vector())]

if (length(dup_genes) > 0) {
    partition_matrix <- partition_matrix[-which(src_genes %in% dup_genes), ]
}

length(partition_matrix)
#as.data.frame(head(partition_matrix))


# >> function: GeosketchExprMatrix
# sketch.size <- 10000
# Just to test
sketch.size <- 100
if (ncol(partition_matrix) > sketch.size) {
    t_partition_matrix <- t(as.data.frame(
        partition_matrix[ , c(2:ncol(partition_matrix)) ]
    ))
    t_partition_matrix[c(1:10), c(1:10)]

    # Get top PCs from randomized SVD.
    s     <- rsvd(t_partition_matrix, k=10)
    X.pcs <- s$u %*% diag(s$d)

    # Sketch 10000 cells
    sketch.indices <- geosketch$gs(X.pcs, sketch.size)

    class(sketch.indices)

    # Grab a slice of the matrix, determined by geosketch; then transpose back
    t_partition_matrix <- t_partition_matrix[unlist(sketch.indices) + 1, ]
    sketched_matrix    <- Table$create(as.data.frame(t(t_partition_matrix)))
}


# ------------------------------
# Inline Archive

# mtx_matrix <- skyteth_r_readers$

# actual file paths
# mtx_filepath <- paste(mtx_basename, 'matrix.mtx', sep='_')
# col_filepath <- paste(mtx_basename, 'mtx_cols'  , sep='.')
# row_filepath <- paste(mtx_basename, 'mtx_rows'  , sep='.')
# 
# mtx_data <- readMM(mtx_filepath)
# mtx_cols <- read.delim(col_filepath, header=FALSE, stringsAsFactors=FALSE)
# mtx_rows <- read.delim(row_filepath, header=FALSE, stringsAsFactors=FALSE)
# 
# colnames(mtx_data) <- mtx_cols$V1
# rownames(mtx_data) <- mtx_rows$V1
# 
# mtx_matrix <- Table$create(as.matrix(
#      mtx_data
#     ,dimnames=list(
#           rownames(mtx_data)
#          ,colnames(mtx_data)
#      )
# ))
