# ------------------------------
# Dependencies

# >> Use R6 to define interface to skyhook
library(R6)

# >> Integrate with skyhook using a python library (for now)
skytether <- import('skytether')


# ------------------------------
# Functions

WritePartition <- function () {
}

WriteDataset <- function (filepath_prefix, gene_expr) {
    # Write dataset containing sparse gene expression data
    data_filepath <- paste(filepath_prefix, 'matrix.mtx', sep='_')
    writeMM(gene_expr, file=data_filepath)

    # Write file containing column names (cells)
    write.table(
         colnames(gene_expr)
        ,file=paste(filepath_prefix, 'cells.tsv', sep='_')
        ,row.names=FALSE
        ,col.names=FALSE
    )

    # Write file containing row names (genes)
    write.table(
         rownames(gene_expr)
        ,file=paste(filepath_prefix, 'genes.tsv', sep='_')
        ,row.names=FALSE
        ,col.names=FALSE
    )

    data_filepath
}


# ------------------------------
# Classes

# >> General skyhook interfaces

SkyhookIndex <- R6Class(
     'SkyhookIndex'
    ,public=list(
         # Attributes
         
     )
)

HashIndex <- R6Class(
     'HashIndex'
    ,public=list(
         # Attributes
     )
)

# SkyhookDataset should be used to define the desired properties of the user's dataset. That is,
# it needs a partition key to identify a storage object, etc.
SkyhookDataset <- R6Class(
     'SkyhookDataset'
    ,public=list(
          # Attributes
          partition_key=NULL

          # Functions
         ,initialize=function (partition_key=NA) {
              self$partition_key <- partition_key
          }
         ,
     )
)

# >> Domain-specific interfaces

# single-cell gene expression dataset
SingleCellExpression <- R6Class(
     'SingleCellExpression'
    ,inherit=SkyhookDataset
    ,public=list(
          # Attributes
          partition_key=NULL

          # Functions
         ,initialize=function (partition_key=NA) {
              self$partition_key <- partition_key
          }
         ,
     )
)

# single-cell gene expression dataset
SingleCellClusters <- R6Class(
     'SingleCellClusters'
    ,inherit=SkyhookDataset
    ,public=list(
          # Attributes
          partition_key=NULL

          # Functions
         ,initialize=function (partition_key=NA) {
              self$partition_key <- partition_key
          }
         ,
     )
)
