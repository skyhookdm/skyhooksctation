# ------------------------------
# Overview

# A very simple load that sets up our kinetic drive to be a repository of expression matrices.


# ------------------------------
# Dependencies

library(arrow)
library(Matrix)
library(reticulate)

skyteth_r <- import('skytether.skyteth_r')


# ------------------------------
# Reference variables

dataset_prefix       <- file.path('/home/drin', 'code', 'research', 'skyhooksctation')
path_to_ebi_datasets <- file.path(dataset_prefix, 'resources', 'sample-data', 'ebi')
path_to_staging      <- file.path(dataset_prefix, 'resources', 'staged-data', 'ebi')

partition_keyname    <- 'partition_name'


# ------------------------------
# Functions

# Skip the 'centroids' folder by default
DatasetsFromPath <- function (dir_path, skip.entry='centroids') {
    dir_entries   <- file.path(dir_path, list.files(path=dir_path))

    # grep finds an index and <vector>[-<ndx>] removes an element by position
    dir_entries[-grep(skip.entry, dir_entries, fixed=TRUE)]
}

ReadGeneExpr <- function (data_path) {
    f.umi     <- list.files(path=data_path, pattern="filtered_counts.*mtx$"    , full.names=TRUE)
    f.gene    <- list.files(path=data_path, pattern="filtered_counts.mtx_rows$", full.names=TRUE)
    f.barcode <- list.files(path=data_path, pattern="filtered_counts.mtx_cols$", full.names=TRUE)

    # f.meta    <- list.files(path = ".", pattern = "*ExpDesign",full.names = TRUE)

    # read in gene expression matrix
    umi           <- readMM(file = f.umi)
    gene.names    <- read.delim(f.gene   , header=FALSE, stringsAsFactors=FALSE)
    barcode.names <- read.delim(f.barcode, header=FALSE, stringsAsFactors=FALSE)
    colnames(umi) <- barcode.names$V1
    rownames(umi) <- gene.names$V1
    # metadata      <- read.delim(f.meta,header = FALSE, stringsAsFactors = FALSE)

    as.matrix(umi)
}

ArrowTableFromMatrix <- function (partition_name, expr_matrix) {
    expr_schema_fields <- list()
    expr_schema_fields[colnames(expr_matrix)] <- c(uint32())

    # saving these, because we tend to lose them after an apply
    expr_rowids <- rownames(expr_matrix)

    expr_matrix <- apply(expr_matrix, 2, as.integer)
    rownames(expr_matrix) <- expr_rowids

    expr_table <- Table$create(as.data.frame(expr_matrix), schema=schema(!!!expr_schema_fields))
    expr_table$metadata[partition_keyname] <- partition_name
}


# ------------------------------
# Main Logic

if (! file.exists(path_to_ebi_datasets)) {
    message('Path does not exist: ', path_to_ebi_datasets)
    quit(no)
}

# dataset dirs are folders for each EBI dataset
for (ebi_dataset_dir in DatasetsFromPath(path_to_ebi_datasets)) {
    dataset_name <- basename(ebi_dataset_dir)
    message('Loading dataset: ', dataset_name)

    # >> Read gene expression into memory and remove duplicated genes (rows)
    umi       <- ReadGeneExpr(data_path=ebi_dataset_dir)
    umi_table <- ArrowTableFromMatrix(dataset_name, umi)

    py_expr_table <- skyteth_r$GeneExpression(data=umi_table)
    py_expr_table$WriteAll(partition_namekey=partition_keyname, batch_size=10240)

    break
}
