# This script is for a mock up differential gene expression analysis between two meta-clusters. Gene expression vectors are retrieved from each cell(from clustering files)
#example of comparing meta-cluster 12(14 centroids) and 13(6 centroids)
library(Matrix)

# quantile_normalisation <- function(df){
#   df_rank <- apply(df,2,rank,ties.method="min")
#   df_sorted <- data.frame(apply(df, 2, sort))
#   df_mean <- apply(df_sorted, 1, mean)
#   
#   index_to_mean <- function(my_index, my_mean){
#     return(my_mean[my_index])
#   }
#   
#   df_final <- apply(df_rank, 2, index_to_mean, my_mean=df_mean)
#   rownames(df_final) <- rownames(df)
#   return(df_final)
# }

EBI.centroids <- read.delim("/public/groups/stuartlab/single-cell-datasets/EBI/centroids/EBI_louvain_centroids.tsv", sep=" ")
membership <- read.delim("/public/groups/stuartlab/single-cell-datasets/EBI/centroids/EBI_louvain_scBeacon_result_membership.tsv")
table <- read.delim("/public/groups/stuartlab/single-cell-datasets/EBI/centroids/gene_id_converter_table.tsv")
meta.clusters <- c(12, 13)
#extract expression matrices in each meta-cluster
# Note: to compare two meta-clusters come from different datasets, we might need extra normalization 
setwd("/public/groups/stuartlab/single-cell-datasets/EBI")
meta.exp <- list()
for (meta.cluster in meta.clusters){
  message(meta.cluster)
  cluster.centroids <- rownames(membership)[membership[,"cluster"] == meta.cluster]
  cluster.exp <- list()
  for (centroid in cluster.centroids){
    message(centroid)
    dataset <- unlist(strsplit(centroid, split="_"))[1]
    setwd(paste("./", dataset, "/louvain", sep=""))
    f <- paste(centroid, "matrix.mtx", sep="_")
    genes <- read.delim(gsub("matrix.mtx", "genes.tsv", f), header = F, stringsAsFactors = F)
    cells <- read.delim(gsub("matrix.mtx", "cells.tsv", f), header = F, stringsAsFactors = F)
    exp <- as.matrix(readMM(f))
    rownames(exp) <- genes$V1
    colnames(exp) <- cells$V1
    id <- which.max(apply(table, 2, function(x, y) sum(x %in% y), y = rownames(exp)))
    exp <- exp[rownames(exp) %in% table[, id], ]
    rownames(exp) <- table$external_gene_name[match(rownames(exp), table[, id])]
    full.exp <- structure(matrix(0, nrow(table), ncol(exp)), dimnames = list(table$external_gene_name, colnames(exp)))
    full.exp[rownames(exp), ] <- exp
    cluster.exp[[centroid]] <- full.exp
    setwd("../../")
  }
  cluster.exp <- do.call(cbind, cluster.exp)
  meta.exp[[meta.cluster]] <- cluster.exp
}


# cluster.exp.normalized <- normalize.quantiles(cluster.exp)
# cluster.exp.ranked <- apply(cluster.exp, 2, rank)/nrow(cluster.exp)
# annotation <- data.frame(unlist(lapply(names(unlist(annotation)), function(x) unlist(strsplit(x, split="_"))[1])), row.names=as.character(unlist(annotation)))
# library(Rtsne)
# tsne <- structure(Rtsne(as.dist(1-cor(cluster.exp.ranked)), perplexity = floor((ncol(cluster.exp.ranked) - 1)/3))$Y, dimnames = list(colnames(cluster.exp.ranked), c("tSNE1","tSNE2")))
# plot(tsne, pch = "*", cex = 2.5, xlab = "tSNE-1", ylab = "tSNE-2", col = getcol(3)[as.factor(annotation[,1])], cex.axis = 1, cex.lab = 1, cex.main = 1.5) 

#differential expression test eg. t-test
p.value.list <- list()
for (gene in table[,"external_gene_name"]){
  ttest <- t.test(meta.exp[[meta.clusters[1]]][gene,], meta.exp[[meta.clusters[2]]][gene,])
  p.value <- ttest$p.value
  p.value.list[[gene]] <- p.value
}
p.values <- data.frame(p.value.list) #put all the raw p values into a list
FDR <- 0.01
adjusted.p.value <- p.adjust(as.numeric(p.values)[!is.na(as.numeric(p.values))], method="BH") #perform multi-test correction and remove NA values
differential.genes <- table[!is.na(as.numeric(p.values)),"external_gene_name"][adjusted.p.value <= FDR] #extract genes that pass the FDR threshold


