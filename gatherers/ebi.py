"""
Module for downloading experiment data from EBI's gene expression atlas.
"""

# ------------------------------
# Dependencies

import os

import datetime
import json
import requests

from operator import itemgetter


# ------------------------------
# Module variables

# excerpt of JSON that `experiment_dir_json` responds with
example_json = {
    # There are many 'experiment' entries
    "experiments": [
        {
             "experimentAccession"   : "E-CURD-10"
            ,"experimentDescription" : "Single-cell transcriptome profiling for metastatic renal cell carcinoma patient-derived cells and xenografts"
            ,"species"               : "Homo sapiens"
            ,"kingdom"               : "animals"
            ,"loadDate"              : "25-09-2019"
            ,"lastUpdate"            : "25-09-2019"
            ,"numberOfAssays"        : 118
            ,"rawExperimentType"     : "SINGLE_CELL_RNASEQ_MRNA_BASELINE"
            ,"experimentType"        : "Baseline"
            ,"technologyType"        : [ "smart-like" ]
            ,"experimentalFactors"   : [
                 "single cell identifier"
                ,"growth condition"
             ]
            ,"experimentProjects"    :[]
        },
    ]
}


# ------------------------------
# Classes


class Gatherer:
    content_chunksize = 1024 * 1024 * 1024  # 1 MB (in bytes)

    @classmethod
    def download_data(cls, data_uri, data_filepath):
        download_response = requests.get(data_uri, stream=True)
        if download_response.status_code != 200: return False

        if os.path.exists(data_filepath):
            print(f'File already downloaded: "{data_filepath}"')

        else:
            with open(data_filepath, 'wb') as cluster_filehandle:
                for content_chunk in download_response.iter_content(cls.content_chunksize):
                    cluster_filehandle.write(content_chunk)

        return True

    @classmethod
    def get_experiment_meta(cls):
        return requests.get(
            os.path.join(cls.host_url, cls.endpoint_experimentmeta)
        )

    @classmethod
    def get_experiment_data(cls, experiment_list, **kwargs):
        return [
            cls.to_downloaduri(experiment_data, **kwargs)
            for experiment_data in experiment_list
        ]


class EBIGatherer(Gatherer):

    host_url                = 'https://www.ebi.ac.uk'   # Domain Name
    endpoint_experimentdata = 'gxa/sc/experiment'       # for data downloads
    endpoint_experimentmeta = 'gxa/sc/json/experiments' # for JSON response

    # Hard-coded metadata specific to each file type to download
    download_dirpath  = os.path.join('resources', 'downloads', 'EBI')
    filetype_metadata = {
         'clustering': {
              'uri_suffix'   : 'download?fileType=cluster&accessKey='
             ,'dirpath'      : os.path.join(download_dirpath, 'clusters')
             ,'template_file': '{}.clusters.tsv'
         }

        ,'marker-genes': {
              'uri_suffix'   : 'download/zip?fileType=marker-genes&accessKey='
             ,'dirpath'      : os.path.join(download_dirpath, 'marker-genes')
             ,'template_file': '{}.marker-genes-files.zip'
         }

        ,'normalized-counts': {
              'uri_suffix'   : 'download/zip?fileType=normalised&accessKey='
             ,'dirpath'      : os.path.join(download_dirpath, 'abundance')
             ,'template_file': '{}.normalised-files.zip'
         }

        ,'experiment-metadata': {
              'uri_suffix'   : 'download/zip?fileType=experiment-metadata&accessKey='
             ,'dirpath'      : os.path.join(download_dirpath, 'experiment-meta')
             ,'template_file': '{}.experiment-metadata-files.zip'
         }

        ,'experiment-design': {
              'uri_suffix'   : 'download?fileType=experiment-design&accessKey='
             ,'dirpath'      : os.path.join(download_dirpath, 'experiment-design')
             ,'template_file': 'ExpDesign-{}.tsv'
         }
    }

    # reference variables
    get_datefields      = itemgetter(2, 1, 0)  # dd-mm-yyyy -> yyyy-mm-dd
    get_datatype_fields = itemgetter('uri_suffix', 'dirpath', 'template_file')


    @classmethod
    def to_downloaduri(cls, experiment_accession, uri_suffix):
        """
            This function construct a URL to download a file. An example URL:
                https://www.ebi.ac.uk/gxa/sc/experiment/E-HCAD-9/download?fileType=experiment-design&accessKey=

            Notice that everything up to the experiment accession ('E-HCAD-9' in this case) can be
            constructed with :ebi_host_url: and :experiment_dir: module variables. The rest of the URL
            is determined by which file to download (e.g. clustering, normalized counts, etc.) and can
            be determined by a lookup of the :urlsuffix_by_filetype: module variable (dictionary).
        """

        # validate parameters
        if (not experiment_accession or not uri_suffix): return None

        return os.path.join(
             cls.host_url
            ,cls.endpoint_experimentdata
            ,experiment_accession
            ,uri_suffix
        )

    @classmethod
    def experiment_list_fromjson(cls, json_str=None):
        """
            Parses JSON received from the :experiment_dir_json: module variable in order to discover
            experiment accessions. There is additional metadata that can be leveraged, but naive use
            case is to just look up all experiment accessions and download any that have not been
            downloaded before.
        """

        if json_str is None:
            metadata_response = cls.get_experiment_meta()
            if metadata_response.status_code != 200: return []

            json_str = metadata_response.text

        return json.loads(json_str).get('experiments', [])

    @classmethod
    def download_by_datatype(cls, experiment_list, datatype):
        datatype_metadata            = cls.filetype_metadata.get(datatype, {})
        urisuffix, dirpath, filename = cls.get_datatype_fields(datatype_metadata)

        for experiment_data in experiment_list:
            acc_id          = experiment_data.get("experimentAccession")
            download_uri    = cls.to_downloaduri(acc_id, urisuffix)
            result_filepath = os.path.join(dirpath, filename.format(acc_id))

            cls.download_data(download_uri, result_filepath)

    @classmethod
    def download_clusters(cls, experiment_list):
        cls.download_by_datatype(experiment_list, 'clustering')

    @classmethod
    def download_counts(cls, experiment_list):
        cls.download_by_datatype(experiment_list, 'normalized-counts')


if __name__ == '__main__':
    ebi_experiment_list = EBIGatherer.experiment_list_fromjson()

    EBIGatherer.download_clusters(ebi_experiment_list)
    EBIGatherer.download_counts(ebi_experiment_list)
