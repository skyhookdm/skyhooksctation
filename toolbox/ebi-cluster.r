# ------------------------------
# Overview


# ------------------------------
# Dependencies
library(Matrix)
library(Seurat)

library(arrow)
library(rsvd)

library(reticulate)
skyteth_r <- import('skytether.skyteth_r')

# ------------------------------
# Functions


ReadGeneExpr <- function (data_path) {
    f.umi     <- list.files(path=data_path, pattern="filtered_counts.*mtx$"    , full.names=TRUE)
    f.gene    <- list.files(path=data_path, pattern="filtered_counts.mtx_rows$", full.names=TRUE)
    f.barcode <- list.files(path=data_path, pattern="filtered_counts.mtx_cols$", full.names=TRUE)

    # f.meta    <- list.files(path = ".", pattern = "*ExpDesign",full.names = TRUE)
    # metadata      <- read.delim(f.meta,header = FALSE, stringsAsFactors = FALSE)

    # read in gene expression matrix
    umi           <- readMM(file = f.umi)
    rownames(umi) <- read.delim(f.gene   , header=FALSE, stringsAsFactors=FALSE)$V1
    colnames(umi) <- read.delim(f.barcode, header=FALSE, stringsAsFactors=FALSE)$V1

    as.matrix(umi)
}


# ------------------------------
# Cluster gene expression using Louvain algorithm from the Seurat package
ClusterExprWithLouvain <- function (expr_matrix) {
    # initialize Seurat object
    tpm <- CreateSeuratObject(counts=expr_matrix)

    # normalize expression into tpm
    if (sum(GetAssayData(object=tpm)) %% 1 == 0) {
        tpm <- NormalizeData(
             tpm
            ,normalization.method="LogNormalize"
            ,scale.factor=1000000
            ,verbose=FALSE
        )
    }

    genes     <- rownames(tpm)

    # "Scale" selected features and center them, but scale and center are set to FALSE?
    tpm       <- ScaleData(tpm, features=genes, verbose=FALSE, do.scale=FALSE, do.center=FALSE)
    tpm       <- FindVariableFeatures(tpm, selection.method="vst", nfeatures=2000, verbose=FALSE)

    # Run PCA and nearest neighbors (10?)
    tpm       <- RunPCA(tpm, verbose=FALSE)
    tpm       <- FindNeighbors(tpm, dims=1:10, verbose=FALSE)

    tpm       <- FindClusters(tpm, resolution=0.5, verbose=FALSE)

    # Return seurat identities with updated cluster information as the result
    Idents(tpm)
}


ExprAsArrow <- function(expr_matrix) {
    # Transform to Arrow
    data_table <- Table$create(as.data.frame(expr_matrix))
    data_table$metadata$rowndx_style           <- 'r-lang'
    data_table$metadata[rownames(expr_matrix)] <- c(1:nrow(expr_matrix))

    cells_table <- Table$create(
         data.frame(cells=colnames(expr_matrix))
        ,schema=schema(cells=string())
    )

    genes_table <- Table$create(
         data.frame(genes=rownames(expr_matrix))
        ,schema=schema(genes=string())
    )

    # >> Return a python object, GeneExpression, to make writing to kinetic easier
    skyteth_r$GeneExpression(data_table, genes_table, cells_table)
}


# >> Function that is a simple wrapper around a paste command. However, this function provides
#    reasonable defaults for everything except lab and cluster, which are derived values.
MetadataDescriptor <- function ( lab, cell_cluster, platform='Platform', species='hsapiens'
                                ,dstage='DevelopmentalStage', batch=1, tissue='Tissue') {
    paste(lab, platform, species, dstage, batch, tissue, cell_cluster, sep='_')
}


WriteExprAsMTX <- function (filepath_prefix, gene_expr) {
    # Write file containing sparse gene expression data
    data_filepath <- paste(filepath_prefix, 'matrix.mtx', sep='_')
    writeMM(gene_expr, file=data_filepath)

    # Write file containing column names (cells)
    write.table(
         colnames(gene_expr)
        ,file=paste(filepath_prefix, 'cells.tsv', sep='_')
        ,row.names=FALSE
        ,col.names=FALSE
        ,quote=FALSE
    )

    # Write file containing row names (genes)
    write.table(
         rownames(gene_expr)
        ,file=paste(filepath_prefix, 'genes.tsv', sep='_')
        ,row.names=FALSE
        ,col.names=FALSE
        ,quote=FALSE
    )

    data_filepath
}

WriteExprAsArrow <- function (filepath_prefix, gene_expr) {
    # Write file containing sparse gene expression data
    data_filepath <- paste(filepath_prefix, 'matrix.lz4.arrow', sep='_')

    # Transform to Arrow
    data_table <- Table$create(as.data.frame(gene_expr))
    data_table$metadata$rowndx_style         <- 'r-lang'
    data_table$metadata[rownames(gene_expr)] <- c(1:nrow(gene_expr))

    # Write to uncompressed feather format (aka, IPC).
    # Default batch size is 64K; also, compare uncompressed to default lz4
    # write_feather(data_table, sink=data_filepath, compression='uncompressed')
    write_feather(data_table, sink=data_filepath)

    data_filepath
}


# ------------------------------
# Main Logic

# >> Prep

# Prep prefix for output path
# NOTE: dataset_prefix is the checkout location of this repo.
dataset_prefix  <- file.path('.')
path_to_staging <- file.path(dataset_prefix, 'resources', 'staged-data', 'ebi')

# From CLI args, get path to EBI dataset
cli_args        <- commandArgs(trailingOnly=TRUE)
ebi_dataset_dir <- cli_args[1]

if (! file.exists(ebi_dataset_dir)) {
    message('Path does not exist: ', ebi_dataset_dir)
    quit('no')
} else {
    message('Processing: ', ebi_dataset_dir)
}

# >> Read gene expression into memory and cluster
umi           <- ReadGeneExpr(data_path=ebi_dataset_dir)
cell_clusters <- ClusterExprWithLouvain(umi)

# >> Convert to arrow types


# >> Prep clustering outputs
dataset_name      <- basename(ebi_dataset_dir)
clusters_filepath <- file.path(path_to_staging, 'louvain', dataset_name)

dir.create(clusters_filepath, recursive=TRUE)
setwd(clusters_filepath)

# >> Write cluster results for this expression matrix
message('number of clusters in ', dataset_name, ' : ', length(levels(cell_clusters)))
for (cluster_id in levels(cell_clusters)) {
    message('cluster ID: ', cluster_id)

    expr_for_cluster <- cell_clusters[cell_clusters == cluster_id]

    filename_metadata <- MetadataDescriptor(
         lab=dataset_name
        ,cell_cluster=paste(cluster_id, length(expr_for_cluster), sep='_')
    )
    message('filename metadata: ', filename_metadata)

    cluster_mat    <- umi[, names(expr_for_cluster)]
    cluster_dgtmat <- Matrix(cluster_mat, sparse=TRUE)

    expr_mtxfilepath    <- WriteExprAsMTX(filename_metadata, cluster_dgtmat)
    expr_arrowfilepath  <- WriteExprAsArrow(filename_metadata, cluster_dgtmat)

    message('processed: [ ', expr_mtxfilepath, ', ', expr_arrowfilepath, ' ]')
}
