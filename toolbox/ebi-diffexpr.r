# ------------------------------
# Dependencies

library(Matrix)
library(arrow)
library(reticulate)

skyteth_r <- import('skytether.skyteth_r')


# ------------------------------
# Functions

ReadGeneExpr <- function (data_path, pattern='aggregated_filtered_counts') {
    dataset_name <- basename(data_path)
    mtx_prefix   <- file.path(data_path, paste(dataset_name, pattern, sep='.'))

    mtx_filepath  <- paste(mtx_prefix, 'matrix.mtx', sep='_')
    rows_filepath <- paste(mtx_prefix, 'mtx_rows', sep='.')
    cols_filepath <- paste(mtx_prefix, 'mtx_cols', sep='.')

    mtx           <- readMM(mtx_filepath)
    rownames(mtx) <- read.delim(rows_filepath, header=FALSE, stringsAsFactors=FALSE)$V1
    colnames(mtx) <- read.delim(cols_filepath, header=FALSE, stringsAsFactors=FALSE)$V1

    as.matrix(mtx)
}


ExprAsArrow <- function(expr_matrix) {
    # Transform to Arrow
    data_table <- Table$create(as.data.frame(expr_matrix))
    data_table$metadata$rowndx_style           <- 'r-lang'
    data_table$metadata[rownames(expr_matrix)] <- c(1:nrow(expr_matrix))

    cells_table <- Table$create(
         data.frame(cells=colnames(expr_matrix))
        ,schema=schema(cells=string())
    )

    genes_table <- Table$create(
         data.frame(genes=rownames(expr_matrix))
        ,schema=schema(genes=string())
    )

    # >> Return a python object, GeneExpression, to make writing to kinetic easier
    skyteth_r$GeneExpression(data_table, genes_table, cells_table)
}


# ------------------------------
# Main Logic

dataset_prefix       <- file.path('/home/drin', 'code', 'research', 'skyhooksctation')
path_to_ebi_datasets <- file.path(dataset_prefix, 'resources', 'sample-data', 'ebi')
path_to_staging      <- file.path(dataset_prefix, 'resources', 'staged-data', 'ebi')

# From CLI args, get path to EBI dataset
cli_args        <- commandArgs(trailingOnly=TRUE)
ebi_dataset_dir <- cli_args[1]

if (! file.exists(ebi_dataset_dir)) {
    message('Path does not exist: ', ebi_dataset_dir)
    quit('no')
} else {
    message('Processing: ', ebi_dataset_dir)
}

umi  <- ReadGeneExpr(data_path=ebi_dataset_dir)
expr <- ExprAsArrow(umi)

sample_one           <- as.data.frame(expr$data[c(1:5) , c(1:5)])
rownames(sample_one) <- rownames(umi)[c(1:5)]

sample_two           <- as.data.frame(expr$data[c(1:5), c(6:10)])
rownames(sample_two) <- rownames(umi)[c(1:5)]

# show the data
sample_one
sample_two

# show the t-test results
simple_df         <- ncol(sample_one) + ncol(sample_two) - 2
ttest_numerator   <- (apply(sample_one, 1, mean) - apply(sample_two, 1, mean))
ttest_denom_covar <- (
    (
       (ncol(sample_one) - 1) * (apply(sample_one, 1, sd) ** 2)
     + (ncol(sample_two) - 1) * (apply(sample_two, 1, sd) ** 2)
    )
    / (ncol(sample_one) + ncol(sample_two) - 2)
)
ttest_denom_coeff <- sqrt((1 / ncol(sample_one)) + (1 / ncol(sample_two)))
calculated_ttests <- (ttest_numerator / (sqrt(ttest_denom_covar) * ttest_denom_coeff))

message('stats for sample one:')
apply(sample_one, 1, mean)
apply(sample_one, 1, var)


ndx <- 1
for (gene_id in rownames(sample_one)) {
    message('t-test for gene [', gene_id, ']:')

    expected <- t.test(sample_one[gene_id, ], sample_two[gene_id, ])
    message('\tt-test info:\n\t\t', expected$parameter, '\n----')

    message('\t [Expected]   |>  ', expected$statistic, ' (', expected$p.value, ')')

    tval <- calculated_ttests[ndx]
    message('\t [Calculated] |>  ', tval, ' (', 2 * pt(q=tval, df=expected$parameter, lower.tail=FALSE), ')')
    message('\t [Simple]     |>  ', tval, ' (', 2 * pt(q=tval, df=simple_df         , lower.tail=FALSE), ')')

    ndx <- ndx + 1
}
