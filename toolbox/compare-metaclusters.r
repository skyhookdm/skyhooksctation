# ------------------------------
# Overview

# This script is for a mock up differential gene expression analysis between two meta-clusters.
# Gene expression vectors are retrieved from each cell(from clustering files)
# example of comparing meta-cluster 12(14 centroids) and 13(6 centroids)


# ------------------------------
# Dependencies

library(Matrix)
library(preprocessCore)


# ------------------------------
# Reference variables
centroid_dirpath <- file.path('resources', 'sample-data', 'ebi', 'centroids')
clusters_dirpath <- file.path('resources', 'staged-data', 'ebi', 'louvain')

# >> Read reference files
centroid_expr <- read.delim(file.path(centroid_dirpath, 'EBI_louvain_centroids.tsv'), sep=' ')
metaclusters  <- read.delim(file.path(centroid_dirpath, 'EBI_louvain_scBeacon_result_membership.tsv'))
gene_symbols  <- read.delim(file.path(centroid_dirpath, 'gene_id_converter_table.tsv'))


# ------------------------------
# Functions

ReadGeneExpr <- function (cluster_name) {
    # >> determine file paths
    matrix_filepath <- paste(cluster_name, 'matrix.mtx', sep='_')
    rows_filepath   <- paste(cluster_name, 'genes.tsv' , sep='_')
    cols_filepath   <- paste(cluster_name, 'cells.tsv' , sep='_')

    # debug
    message('Matrix data from: ', matrix_filepath)

    # >> read files
    expr            <- as.matrix(readMM(matrix_filepath))
    rownames(expr)  <- read.delim(rows_filepath, header=F, stringsAsFactors=F)$V1
    colnames(expr)  <- read.delim(cols_filepath, header=F, stringsAsFactors=F)$V1

    as.matrix(expr)
}

ConvertGeneIDs <- function (gene_ids, src_namespace, dest_namespace) {
    # match returns a vector, co-indexed with gene_ids, of indices in src_namespace with matching
    # values, and nomatch will return a 0. Then, these indices can be used to access dest_namespace
    dest_namespace[match(gene_ids, src_namespace, nomatch=0)]
}

GeneIDsToHGNC <- function (expr_by_gene) {
    # first, only keep rows for genes that we know about
    known_geneexpr <- expr_by_gene[rownames(expr_by_gene) %in% gene_symbols$ensembl_gene_id, ]

    # then, convert the remaining gene IDs to gene names
    rownames(known_geneexpr) <- ConvertGeneIDs(rownames(known_geneexpr)
        ,src_namespace=gene_symbols$ensembl_gene_id
        ,dest_namespace=gene_symbols$external_gene_name
    )

    known_geneexpr
}

GeneIDsToEnsembl <- function (expr_by_gene) {
    expr_genenames <- rownames(expr_by_gene)

    # first, drop rows where we don't know what the genes are
    expr_by_gene[expr_genenames %in% gene_symbols$external_gene_name]

    # then, convert the gene names to gene IDs
    rownames(expr_by_gene) <- ConvertGeneIDs(expr_genenames
        ,src_namespace=gene_symbols$external_gene_name
        ,dest_namespace=gene_symbols$ensembl_gene_id
    )

    expr_by_gene
}


# ------------------------------
# Main Logic

# >> Check on meta cluster 12 and 13
metacluster_exprs   <- list()
target_metaclusters <- c(12, 13)
for (target_metacluster in target_metaclusters) {
    message('Retrieving expression for metacluster: ', target_metacluster)

    cluster_exprs <- list()
    cluster_names <- (
        rownames(metaclusters)[metaclusters[, 'cluster'] == target_metacluster]
    )

    working_dirpath <- getwd()
    for (cluster_name in cluster_names) {
        message('\tRetrieving expression from cluster: ', cluster_name)

        # >> get source partition name so that we can access it's data from the FS
        dataset_name <- unlist(strsplit(cluster_name, split='_'))[1]
        setwd(file.path(clusters_dirpath, dataset_name))

        # >> Read all of expression, then update gene symbols (row names)
        expr <- ReadGeneExpr(cluster_name)
        expr <- GeneIDsToHGNC(expr)

        # >> Define a dense matrix (a row for every known gene symbol) with dimension names
        expr_for_cluster <- structure(
             matrix(0, nrow(gene_symbols), ncol(expr))
            ,dimnames=list(gene_symbols$external_gene_name, colnames(expr))
        )

        # >> Set expression values for this cluster,
        #    then add the matrix to a list for the metacluster
        expr_for_cluster[rownames(expr), ] <- expr
        cluster_exprs[[cluster_name]]      <- expr_for_cluster

        setwd(working_dirpath)
    }

    # do.call allows us to provide cluster_exprs as a list
    metacluster_exprs[[target_metacluster]] <- do.call(cbind, cluster_exprs)

    break
}

sample_cluster_df <- cluster_exprs[['E-GEOD-100618_Platform_hsapiens_DevelopmentalStage_1_Tissue_0_124']]

sample_cluster_df[c(1:10), c(1:10)]
dim(sample_cluster_df)
